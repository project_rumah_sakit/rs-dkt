<?php include 'header.php'; ?>

<!-- Content area -->
<div class="content">

  <!-- Traffic sources -->
  <div class="panel panel-flat">
    <div class="panel-heading">
      <h4 class="panel-title">Registrasi Pasien</h4>
      <p class="text-muted">Silahkan isi data diri berikut dengan lengkap</p>
      <div class="heading-elements">

      </div>
    </div>
    <div class="panel-body">

      <br/>    

      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="table-responsive"> 
            <form action="registrasi_pasien_act.php" method="post">
              <table class="table table-bordered">                
                <tr>
                  <th width="20%">Username</th>
                  <td>
                    <input type="text" id='username' name="username" class="form-control" required="required" placeholder="Masukan Username">
                  </td>
                </tr>
                <tr>
                  <th>Password</th>
                  <td>
                    <input type="password" id='password' name="password" class="form-control" placeholder="Masukan Password">
                  </td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td>
                    <input type="email" id='email' name="email" class="form-control" placeholder="Masukan Email">
                  </td>
                </tr>
                <tr>
                  <th>No. Telp/HP</th>
                  <td>
                    <input type="number" id='telp' name="telp" class="form-control" placeholder="Masukan Nomor Telepon">
                  </td>
                </tr>
                <tr>
                  <th></th>
                  <td>
                    <input type="submit" class="btn btn-primary" value="Daftar">
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>

      <br/>
      <br/>
      <br/>

    </div>
  </div>
  <!-- /traffic sources -->



  <!-- Footer -->
  <div class="footer text-muted">

  </div>
  <!-- /footer -->

</div>
<!-- /content area -->






<?php include 'footer.php'; ?>