<?php include 'header.php'; ?>

<!-- Content area -->
<div class="content">


    <div class="panel panel-flat">
        <div class="panel-heading">
            <h4 class="panel-title">Fasilitas & Layanan</h4>           
        </div>

        <div class="panel-body">
            <div class="tabbable nav-tabs-vertical nav-tabs-left">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li><a href="#left-tab1" data-toggle="tab" aria-expanded="false"><i class="icon-menu7 position-left"></i> Rawat Inap</a></li>
                    <li><a href="#left-tab2" data-toggle="tab" aria-expanded="false"><i class="icon-menu7 position-left"></i> Rawat Jalan</a></li>                   
                    <li><a href="#left-tab3" data-toggle="tab" aria-expanded="false"><i class="icon-menu7 position-left"></i> Medical Check Up</a></li>                   
                    <li><a href="#left-tab4" data-toggle="tab" aria-expanded="false"><i class="icon-menu7 position-left"></i> Layanan 24 Jam</a></li>                   
                </ul>

                <div class="tab-content">
                    <div class="tab-pane has-padding active" id="left-tab1">
                        <img src="images/rawat-inap.jpg" class="img-responsive">
                        <p> 
                            Rawat inap merupakan salah satu layanan yang terdapat di semua unit rumah sakit HARAPAN JAYAKARTA. Meski fasilitas yang ditawarkan oleh masing-masing unit memiliki perbedaan, namun fasilitas yang ditawarkan oleh layanan rawat inap di setiap unit rumah sakit HARAPAN JAYAKARTA telah dirancang untuk dapat memberikan kenyamanan terbaik bagi para pasien.
                            Kamar maupun ruangan yang disediakan beragam. Mulai dari kamar kelas VVIP, VVIP Anak, VIP, VIP ANAK, 1 PLUS,  1, 2, 3, dan Kamar Bayi, hingga ruangan ICU / ICCU / HCU yang berfungsi untuk memberikan perawatan khusus pada penderita yang memerlukan perawatan yang lebih intensif, juga ruangan Perinatologi / NICU / PICU yang merupakan ruang rawat inap bagi bayi - bayi baru lahir yang membutuhkan perawatan khusus di Rumah Sakit.
                        </p>
                    </div>

                    <div class="tab-pane has-padding" id="left-tab2">
                        <img src="images/rawat-jalan.jpg" class="img-responsive">
                        <p>                    
                            Layanan Rawat Jalan di HARAPAN JAYAKARTA terbagi menjadi 4 layanan utama, yakni Poliklinik, Rehabilitasi Medik, Hemodialisa dan Dental Health & Aesthetic Clinic.

                            Pada layanan Poliklinik, Anda dapat mengobati berbagai gangguan kesehatan mulai dari kesehatan anak, penyakit dalam, syaraf, kulit & kelamin, psikologi, gizi dan berbagai gangguan kesehatan lainnya.

                            Layanan Rehabilitasi Medik (Fisioterapi) HARAPAN JAYAKARTA merupakan pelayanan kesehatan yang ditujukan terhadap Anda yang mengalami gangguan fisik dan fungsional yang diakibatkan oleh keadaan atau kondisi sakit, penyakit atau cedera melalui panduan intervensi medis, keterapian fisik dan atau rehabilitatif untuk mencapai kemampuan fungsi yang optimal.

                            Jika Anda merupakan pasien yang membutuhkan layanan proses pembersihan darah oleh akumulasi sampah buangan, HARAPAN JAYAKARTA juga memiliki layanan tersebut pada layanan Hemodialisa. Sedangkan untuk Anda yang ingin memiliki gigi sehat dan estetis, HARAPAN JAYAKARTA menyediakan layanan Dental Health & Aesthetic Clinic yang didukung oleh teknologi modern dan spesialisasi kedokteran gigi yang lengkap.

                        </p>
                    </div>

                    <div class="tab-pane has-padding" id="left-tab3">
                        <img src="images/mcu.jpg" class="img-responsive">
                        <p>
                            Mencegah lebih baik daripada mengobati. Kesehatan Anda adalah sesuatu yang tak ternilai harganya. Kesehatan harus Anda jaga sebaik mungkin sebelum penyakit menggerogoti berbagai aspek kehidupan Anda. Sebagai langkah pencegahan, lakukanlah Medical Check Up (MCU) untuk mengetahui kondisi kesehatan Anda, sekaligus mendeteksi suatu penyakit sejak dini.
                            HARAPAN JAYAKARTA menawarkan berbagai paket Medical Check Up yang sesuai dengan kebutuhan kesehatan Anda. Dengan teknologi modern dan tenaga profesional yang sudah berpengalaman di bidangnya, layanan Medical Check Up HARAPAN JAYAKARTA siap memberikan yang terbaik untuk kesehatan Anda.
                        </p>
                    </div>

                    <div class="tab-pane has-padding" id="left-tab4">
                        <img src="images/24.jpg" class="img-responsive">
                       <p>                
                        HARAPAN JAYAKARTA dilengkapi dengan berbagai layanan yang siap melayani Anda selama 24 jam tanpa henti. Ada layanan Laboratorium yang melayani permintaan pemeriksaan dari internal maupun rujukan dari luar HARAPAN JAYAKARTA dan ada juga Instalasi Gawat Darurat (IGD), yang siap melayani pasien dengan ketersediaan dokter IGD dan paramedik setiap saat.
                        Selain Laboratorium dan IGD, ada juga layanan lainnya seperti, Ambulans yang dilengkapi dengan paramedik dan berbagai fasilitas untuk pertolongan terhadap pasien dalam perjalanan, layanan Farmasi dengan apoteker-apoteker profesional dan kompeten di bidangnya, dan juga layanan Radiologi yang merupakan sarana penunjang medis untuk membantu dokter dalam merawat pasien dengan memberikan pelayanan pencitraan diagnostik (Diagnostic Imaging).
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>











<!-- Footer -->
<div class="footer text-muted">

</div>
<!-- /footer -->

</div>
<!-- /content area -->


<?php include 'footer.php'; ?>