<?php include 'header.php'; ?>
<!-- Main content -->
<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

		<!-- Main charts -->
		<div class="row">
			<div class="col-lg-12">
				<!-- Traffic sources -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">Detail</h4>
						<div class="heading-elements">
							<a href="laporan.php" class="btn btn-sm btn-primary">Kembali</a>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<h5>Data Pasien</h5>
							<table class="table table-bordered table-hover table-striped">														
								<?php		
								$id = $_GET['id'];
								$data = mysql_query("select * from pendaftaran,dokter,spesialis where pendaftaran_dokter=dokter_id and dokter_spesialis=spesialis_id and pendaftaran_id='$id'");		
								while($d=mysql_fetch_array($data)){
									?>
									<tr>	
										<th width="20%">Tgl. Berobat</th>									
										<td><?php echo $d['pendaftaran_tgl_berobat'] ?></td>										
									</tr>
									<tr>	
										<th>No. Antrian</th>									
										<td><?php echo $d['pendaftaran_no'] ?></td>										
									</tr>
									<tr>	
										<th>Nama Pasien</th>									
										<td><?php echo $d['pendaftaran_nama'] ?></td>										
									</tr>
									<tr>	
										<th>No. Identitas</th>									
										<td><?php echo $d['pendaftaran_no_identitas'] ?></td>										
									</tr>
									<tr>	
										<th>Alamat</th>									
										<td><?php echo $d['pendaftaran_alamat'] ?></td>										
									</tr>
									<tr>	
										<th>Tempat, Tgl Lahir</th>									
										<td><?php echo $d['pendaftaran_ttl'] ?></td>										
									</tr>
									<tr>	
										<th>Umur Pasien</th>									
										<td><?php echo $d['pendaftaran_umur'] ?> Tahun</td>										
									</tr>
									<tr>	
										<th>No. HP/ Telp</th>									
										<td><?php echo $d['pendaftaran_hp'] ?></td>										
									</tr>									
									<?php
								}
								?>
							</table>
						</div>	
						<br/>
						<div class="table-responsive">
							<h5>Data Dokter & Poli</h5>
							<table class="table table-bordered table-hover table-striped">														
								<?php									
								$data = mysql_query("select * from pendaftaran,dokter,spesialis where pendaftaran_dokter=dokter_id and dokter_spesialis=spesialis_id and pendaftaran_id='$id'");		
								while($d=mysql_fetch_array($data)){
									?>
									<tr>	
										<th width="20%">Nama Dokter</th>									
										<td><?php echo $d['dokter_nama'] ?></td>										
									</tr>
									<tr>	
										<th>Poli / Spesialis</th>									
										<td><?php echo $d['spesialis_nama'] ?></td>										
									</tr>
									<tr>	
										<th>Alamat</th>									
										<td><?php echo $d['dokter_alamat'] ?></td>										
									</tr>																	
									<?php
								}
								?>
							</table>
						</div>			

					</div>					
				</div>	


			</div>

		</div>		

		

	</div>
</div>

<?php include 'footer.php'; ?>