<?php include 'header.php'; ?>
<!-- Main content -->
<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

		<!-- Main charts -->
		<div class="row">
			<div class="col-lg-12">
				<!-- Traffic sources -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">Data jadwal</h4>
						<div class="heading-elements">
							<a href="jadwal_tambah.php" class="btn btn-sm btn-primary"><i class="icon-plus22"></i> TAMBAH</a>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">						
								<tr>
									<th width="1%">No</th>									
									<th width="20%">Nama Dokter</th>		
									<th width="30%">Spesialis</th>		
									<th>Hari</th>		
									<th>Jam</th>		
									<th>Keterangan</th>		
									<th width="15%">OPSI</th>
								</tr>
								<?php
								$no = 1; 
								$data = mysql_query("select * from dokter,spesialis,jadwal where dokter_spesialis=spesialis_id and jadwal_dokter=dokter_id");		
								while($d=mysql_fetch_array($data)){
									?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $d['dokter_nama'] ?></td>
										<td><?php echo $d['spesialis_nama'] ?></td>			
										<td><?php echo $d['jadwal_hari'] ?></td>			
										<td><?php echo $d['jadwal_jam'] ?></td>			
										<td><?php echo $d['jadwal_keterangan'] ?></td>			
										<td>
											<a class="btn border-teal text-teal btn-flat btn-icon btn-xs" href="jadwal_edit.php?id=<?php echo $d['jadwal_id'];?>"><i class="icon-wrench3"></i></a>
											<a class="btn border-danger text-danger btn-flat btn-icon btn-xs" href="jadwal_hapus.php?id=<?php echo $d['jadwal_id'];?>"><i class="icon-trash-alt"></i></a>
										</td>
									</tr>
									<?php
								}
								?>
							</table>
						</div>					
					</div>					
				</div>	


			</div>

		</div>		
	
		<div class="footer text-muted">
			<!-- &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a> -->
		</div>

	</div>
</div>

<?php include 'footer.php'; ?>