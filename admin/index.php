	<?php include 'header.php'; 
	include 'admin/config.php';
	?>
	<!-- Main content -->
	<div class="content-wrapper">

		<!-- Content area -->
		<div class="content">
			
			<!-- Dashboard content -->
			<div class="row">
				<div class="col-lg-12">
					
					<!-- Quick stats boxes -->
					<div class="row">
						<div class="col-lg-4">

							<!-- Members online -->
							<div class="panel bg-teal-400">
								<div class="panel-body">
									<?php $pasien=mysqli_query($conn,"select * from pasien"); ?>
									<h3 class="no-margin"><?php echo mysqli_num_rows($pasien) . " pasien"; ?></h3>
									Jumlah pasien
									
								</div>

								<div class="container-fluid">
									<div id="members-online"></div>
								</div>
							</div>
							<!-- /members online -->

						</div>

						<div class="col-lg-4">

							<!-- Current server load -->
							<div class="panel bg-pink-400">
								<div class="panel-body">
									<?php $dokter=mysqli_query($conn,"select * from dokter"); ?>
									<h3 class="no-margin"><?php echo mysqli_num_rows($dokter) . " dokter"; ?></h3>
									Jumlah dokter
									
								</div>

								<div id="server-load"></div>
							</div>
							<!-- /current server load -->

						</div>

						<div class="col-lg-4">

							<!-- Today's revenue -->
							<div class="panel bg-blue-400">
								<div class="panel-body">									
									<?php $spesialis=mysqli_query($conn,"select * from spesialis"); ?>
									<h3 class="no-margin"><?php echo mysqli_num_rows($spesialis) . " spesialis"; ?></h3>
									Jumlah spesialis								
								</div>

								<div id="today-revenue"></div>
							</div>
							<!-- /today's revenue -->

						</div>
					</div>
					<!-- /quick stats boxes -->			

				</div>

				<div class="col-lg-12">					
					<!-- Daily sales -->
					<div class="panel panel-flat">
						<div class="panel-heading">							
						</div>

						<div class="panel-body">
							<div class="table-responsive">							
								<center>
									<h3>Selamat datang di sistem informasi pendaftaran pasien online <br/> RS. HARAPAN JAYAKARTA</h3>
								</center>
							</div>
						</div>


					</div>
					<!-- /daily sales -->


				</div>
			</div>
			<!-- /dashboard content -->


			<!-- Footer -->
			<div class="footer text-muted">

			</div>
			<!-- /footer -->

		</div>
		<!-- /content area -->

	</div>
	<!-- /main content -->

	<?php include 'footer.php'; ?>