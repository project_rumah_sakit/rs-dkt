<?php include 'header.php'; ?>
<!-- Main content -->
<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

		<!-- Main charts -->
		<div class="row">
			<div class="col-lg-12">
				<!-- Traffic sources -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Data dokter</h4>
						<div class="heading-elements">
							<a href="dokter.php" class="btn btn-sm btn-primary"><i class="icon-arrow-left12"></i> KEMBALI</a>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<form action="dokter_update.php" method="post">
								<?php
								$id = $_GET['id'];
								$data = mysql_query("select * from dokter where dokter_id='$id'");		
								while($d=mysql_fetch_array($data)){
									?>
									<table class="table">
										<tr>
											<th width="20%">Nama Dokter</th>
											<td>
												<input type="hidden" name="id" value="<?php echo $d['dokter_id']; ?>">
												<input type="text" class="form-control" name="nama" required="required" value="<?php echo $d['dokter_nama']; ?>">
											</td>
										</tr>
										<tr>
											<th width="20%">Spesialis</th>
											<td>
												<select name="spesialis" class="form-control" required="required">
													<option value="">-Pilih</option>
													<?php 
													$data2 = mysql_query("select * from spesialis");
													while($d2 = mysql_fetch_array($data2)){
														?>
														<option <?php if($d2['spesialis_id'] == $d['dokter_spesialis']){echo "selected='selected'";} ?> value="<?php echo $d2['spesialis_id']; ?>"><?php echo $d2['spesialis_nama']; ?></option>
														<?php } ?>
													</select>
												</td>
											</tr>									
											<tr>
												<th>Alamat</th>
												<td><input type="text" class="form-control" name="alamat" required="required" value="<?php echo $d['dokter_alamat']; ?>"></td>
											</tr>


											<tr>
												<th></th>
												<td><input type="submit" value="Simpan" class="btn btn-primary btn-sm"></td>
											</tr>		
										</table>
										<?php } ?>
									</form>
								</div>					
							</div>					
						</div>	


					</div>

				</div>		

				<div class="footer text-muted">
					<!-- &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a> -->
				</div>

			</div>
		</div>

		<?php include 'footer.php'; ?>