<?php include 'header.php'; ?>
<!-- Main content -->
<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

		<!-- Main charts -->
		<div class="row">
			<div class="col-lg-12">
				<!-- Traffic sources -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">Tambah Data jadwal</h4>
						<div class="heading-elements">
							<a href="jadwal.php" class="btn btn-sm btn-primary"><i class="icon-arrow-left12"></i> KEMBALI</a>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<form action="jadwal_act.php" method="post">
								<table class="table">									
									<tr>
										<th width="20%">Dokter</th>
										<td>
											<select name="dokter" class="form-control" required="required" onchange="ajaxdokter(this.value)">
												<option value="">-Pilih</option>
												<?php 
												$data = mysql_query("select * from dokter");
												while($d = mysql_fetch_array($data)){
													?>
													<option value="<?php echo $d['dokter_id']; ?>"><?php echo $d['dokter_nama']; ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>		

										<tr>
											<th>Spesialis</th>
											<td><p class="spesialis"> - </p></td>
										</tr>
										<tr>
											<th>Hari</th>
											<td>
												<input type="text" name="hari" class="form-control">
											</td>
										</tr>
										<tr>
											<th>Jam</th>
											<td>
												<input type="text" name="jam" class="form-control">
											</td>
										</tr>
										<tr>
											<th>Keterangan</th>
											<td>
												<input type="text" name="keterangan" class="form-control">
											</td>
										</tr>
										<tr>
											<th></th>
											<td><input type="submit" value="Simpan" class="btn btn-sm btn-primary"></td>
										</tr>		
									</table>
								</form>
							</div>					
						</div>					
					</div>	


				</div>

			</div>		


		</div>
	</div>
	<script type="text/javascript">
		function ajaxdokter(id){					
			var data = "id="+id;
			$.ajax({
				type: 'POST',
				url: "jadwal_tambah_ajax.php",
				data: data,
				success: function(html) {		
					if(html != ""){
						$('.spesialis').html(html);
					}else{
						$('.spesialis').html("-");
					}						
				}
			});
		}

	// $(document).ready(function(){
		// $('body').on("keyup",".id_pel",function(){
			
		// });
	// });
	
</script>
<?php include 'footer.php'; ?>