<?php include 'header.php'; ?>
<!-- Main content -->
<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

		<!-- Main charts -->
		<div class="row">
			<div class="col-lg-12">
				

				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">Filter</h4>						
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<form method="get" action="">
								<table class="table table-bordered table-hover table-striped">						
									<tr>
										<th width="20%">Dokter / Poli</th>	
										<td>
											<select name="dokter" class="form-control" required="required">
												<option value="">-Pilih Dokter/Poli</option>
												<?php 
												$data = mysql_query("select * from dokter");
												while($d = mysql_fetch_array($data)){
													?>
													<option value="<?php echo $d['dokter_id']; ?>"><?php echo $d['dokter_nama']; ?></option>
													<?php 
												}
												?>
											</select>
										</td>																						
									</tr>
									<tr>
										<th width="20%">Tanggal</th>										
										<td><input type="date" name="tanggal" class="form-control" required="required"></td>									
									</tr>
									<tr>																	
										<td></td>
										<td><input type="submit" name="filter" value="Tampilkan" class="btn btn-sm btn-primary"></td>
									</tr>													
								</table>
							</form>
						</div>					
					</div>					
				</div>	

				<?php 
				if(isset($_GET['filter'])){
					$dokter = $_GET['dokter'];
					$tanggal = $_GET['tanggal'];
					?>
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h4 class="panel-title">Laporan Pendaftaran</h4>						
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-bordered table-hover table-striped">						
									<tr>
										<th width="1%">No</th>									
										<th width="20%">Nama Pasien</th>		
										<th>TTL</th>		
										<th>Umur</th>																						
										<th>Dokter</th>																						
										<th>Spesialis / Poli</th>																						
										<th>No. Antrian</th>																						
										<th width="10%">OPSI</th>
									</tr>
									<?php
									$no = 1; 
									$data = mysql_query("select * from pendaftaran,dokter,spesialis where pendaftaran_dokter=dokter_id and dokter_spesialis=spesialis_id and date(pendaftaran_tgl_berobat)='$tanggal' and dokter_id='$dokter' order by pendaftaran_no asc");		
									while($d=mysql_fetch_array($data)){
										?>
										<tr>
											<td><?php echo $no++; ?></td>
											<td><?php echo $d['pendaftaran_nama'] ?></td>
											<td><?php echo $d['pendaftaran_ttl'] ?></td>
											<td><?php echo $d['pendaftaran_umur'] ?> Tahun</td>
											<td><?php echo $d['dokter_nama'] ?></td>
											<td><?php echo $d['spesialis_nama'] ?></td>														
											<td><?php echo $d['pendaftaran_no'] ?></td>														
											<td>												
												<a target="_blank" class="btn border-danger text-danger btn-flat btn-icon btn-xs" href="laporan_detail.php?id=<?php echo $d['pendaftaran_id'];?>"><i class="icon-user"></i> Detail</a>
											</td>
										</tr>
										<?php
									}
									?>
								</table>
							</div>					
						</div>					
					</div>

					<?php
				}
				 ?>
				

			</div>

		</div>		

		<div class="footer text-muted">
			<!-- &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a> -->
		</div>

	</div>
</div>

<?php include 'footer.php'; ?>