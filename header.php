<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> SISTEM INFORMASI PENDAFTARAN PASIEN ONLINE RS. HARAPAN JAYAKARTA</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="assets/css/core.css" rel="stylesheet" type="text/css">
  <link href="assets/css/components.css" rel="stylesheet" type="text/css">
  <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- Core JS files -->
  <!-- <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script> -->
  <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
  <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
  <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
  <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
  <script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
  <script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
  <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

  <script type="text/javascript" src="assets/js/core/app.js"></script>
  <!-- <script type="text/javascript" src="assets/js/pages/dashboard_boxed.js"></script> -->

</head>

<body class="layout-boxed">
  <?php session_start(); ?>
  <?php include 'admin/config.php'; ?>


  <div class="navbar bg-pink-400">
    <br/>
    <br/>
    <center>
      <h3>SISTEM INFORMASI PENDAFTARAN PASIEN ONLINE RS. HARAPAN JAYAKARTA</h3>
    </center>
    <br/>
    <br/>
  </div>

  <!-- Main navbar -->
  <div class="navbar navbar-default">
    <div class="navbar-header">     
      <ul class="nav navbar-nav visible-xs-block">
        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-paragraph-justify3"></i></a></li>     
      </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
      <ul class="nav navbar-nav">       
        <li><a href="index.php"><span>Home</span></a></li>
        <li><a href="profil.php"><span>Profil</span></a></li>
        <li><a href="fasilitas.php">Fasilitas & Layanan</a></li>
        <li><a href="kontak.php">Kontak</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">        
       <li class="dropdown dropdown-user">
        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">                      
          <span>DAFTAR</span>         
          <i class="caret"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">          
          <li><a href="registrasi_pasien.php"><i class="icon-lock"></i> <span>Registrasi Pasien</span></a></li>          
          <li><a href="login_pasien.php"><i class="icon-lock"></i> <span>Login Pasien</span></a></li>          
        </ul>
      </li>     
       <li><a href="login.php"><i class="icon-lock"></i> <span>Login Admin</span></a></li>          
     </ul>


   </div>
 </div>
 <!-- /main navbar -->


 <!-- Page container -->
 <div class="page-container">

  <!-- Page content -->
  <div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">