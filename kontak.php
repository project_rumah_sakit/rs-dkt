<?php include 'header.php'; ?>

<!-- Content area -->
<div class="content">

  <!-- Traffic sources -->
  <div class="panel panel-flat">
    <div class="panel-heading">
      <h4 class="panel-title">Kontak</h4>
      <div class="heading-elements">

      </div>
    </div>
    <div class="panel-body">

     <br/>    
     <div class="table-responsive"> 
       <table class="table table-bordered">
        <tr>
          <th width="1%">Nama</th>
          <td>Rumah Sakit Harapan Jayakarta</td>
        </tr>
        <tr>
          <th>Alamat</th>
          <td>Jl.Bekasi Timur Raya No.6 Km.18, Cakung, Jakarta Timur</td>
        </tr>
        <tr>
          <th>No Telp</th>
          <td>(021) 4601371, 4608886</td>
        </tr>
        <tr>
          <th>Fax</th>
          <td>46088863</td>
        </tr>
        <tr>
          <th>Kepemilikan</th>
          <td>Yayasan</td>
        </tr>
        <tr>
          <th>Type RS</th>
          <td>C</td>
        </tr>
      </table>
    </div>
  </div>
</div>
<!-- /traffic sources -->



<!-- Footer -->
<div class="footer text-muted">

</div>
<!-- /footer -->

</div>
<!-- /content area -->






<?php include 'footer.php'; ?>