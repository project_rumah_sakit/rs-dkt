<?php include 'header.php'; ?>

<!-- Content area -->
<div class="content">

  <!-- Traffic sources -->
  <div class="panel panel-flat">
    <div class="panel-heading">
      <h4 class="panel-title">Login Pasien</h4>
      <p class="text-muted">Silahkan login</p>
      <div class="heading-elements">

      </div>
    </div>
    <div class="panel-body">

  <?php 
      if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "daftar"){
          echo "<div class='alert alert-success'>Anda telah berhasil mendaftar sebagai pasien. selanjutnya silahkan login.</div>";
        }else if($_GET['pesan'] == "gagal"){
          echo "<div class='alert alert-danger'>Login gagal! username dan password tidak sesuai.</div>";
        }
      }
      ?>
      
      <br/>    

      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="table-responsive"> 
            <form action="login_pasien_act.php" method="post">
              <table class="table table-bordered">                
                <tr>
                  <th width="20%">Username</th>
                  <td>
                    <input type="text" id='username' name="username" class="form-control" required="required">
                  </td>
                </tr>
                <tr>
                  <th>Password</th>
                  <td>
                    <input type="password" id='password' name="password" class="form-control">
                  </td>
                </tr>               
                <tr>
                  <th></th>
                  <td>
                    <input type="submit" class="btn btn-primary" value="Login">
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>

      <br/>
      <br/>
      <br/>

    </div>
  </div>
  <!-- /traffic sources -->



  <!-- Footer -->
  <div class="footer text-muted">

  </div>
  <!-- /footer -->

</div>
<!-- /content area -->






<?php include 'footer.php'; ?>