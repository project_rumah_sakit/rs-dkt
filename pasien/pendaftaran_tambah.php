<?php include 'header.php'; ?>
<!-- Main content -->
<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

		<!-- Main charts -->
		<div class="row">
			<div class="col-lg-12">
				<!-- Traffic sources -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">Pendaftaran Berobat Pasien</h4>
						<div class="heading-elements">
							
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<form action="pendaftaran_act.php" method="post">
								<table class="table">
									<tr>
										<th>Nama Pasien</th>
										<td>
											<input type="text" name="nama" class="form-control" required="required" placeholder="Nama lengkap pasien ..">
										</td>
									</tr>
									<tr>
										<th>No. Identitas</th>
										<td>
											<input type="text" name="identitas" class="form-control" required="required" placeholder="No. KTP ..">
										</td>
									</tr>
									<tr>
										<th>Alamat</th>
										<td>
											<input type="text" name="alamat" class="form-control" required="required" placeholder="Alamat ..">
										</td>
									</tr>
									<tr>
										<th>TTL</th>
										<td>
											<input type="text" name="ttl" class="form-control" required="required" placeholder="CONTOH : Bandung, 2/2/1983 ..">
										</td>
									</tr>
									<tr>
										<th>Umur</th>
										<td>
											<input type="text" name="umur" class="form-control" required="required" placeholder="Umur pasien .." style="width: 300px">
										</td>
									</tr>
									<tr>
										<th>No.HP</th>
										<td>
											<input type="text" name="hp" class="form-control" required="required" placeholder="No.HP ..">
										</td>
									</tr>

									<tr>
										<th width="20%">Dokter</th>
										<td>
											<select name="dokter" class="form-control" required="required" onchange="ajaxdokter(this.value)">
												<option value="">-Pilih</option>
												<?php 
												$data = mysql_query("select * from dokter");
												while($d = mysql_fetch_array($data)){
													?>
													<option value="<?php echo $d['dokter_id']; ?>"><?php echo $d['dokter_nama']; ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>	
										<tr>
											<th>Poli/Spesialis</th>
											<td>
												<p class="spesialis">-</p>
											</td>
										</tr>											
										<tr>
											<th>Hari/Tanggal Berobat</th>
											<td>
												<input type="date" name="tgl_berobat" class="form-control" required="required">
											</td>
										</tr>
										<tr>
											<th></th>
											<td><input type="submit" value="Simpan" class="btn btn-sm btn-primary"></td>
										</tr>		
									</table>
								</form>
							</div>					
						</div>					
					</div>	


				</div>

			</div>		


		</div>
	</div>
	<script type="text/javascript">
		function ajaxdokter(id){					
			var data = "id="+id;
			$.ajax({
				type: 'POST',
				url: "jadwal_tambah_ajax.php",
				data: data,
				success: function(html) {		
					if(html != ""){
						$('.spesialis').html(html);
					}else{
						$('.spesialis').html("-");
					}						
				}
			});
		}

	</script>
	<?php include 'footer.php'; ?>